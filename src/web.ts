import { WebPlugin, registerWebPlugin } from '@capacitor/core';
import { SanbotPluginPlugin, SANBOT_INFRARE_EVENT, SanbotEmotion } from './definitions';
import { infraredSensor$ } from './web/infrared-sensor';

export class SanbotPluginWeb extends WebPlugin implements SanbotPluginPlugin {

  constructor() {
    super({
      name: 'SanbotPlugin',
      platforms: ['web']
    });
  }

  init(): Promise<void> {
    return Promise.resolve();
  }

  getDeviceId(): Promise<{deviceId: string}> {
    return Promise.resolve({deviceId: 'test-robot-id'});
  }

  getApiVersion(): Promise<{version: string}> {
    return Promise.resolve({version: 'test-robot-api-version'});
  }

  showEmotion(options: {emotion: SanbotEmotion}): Promise<void> {
    console.log(`Showing emotion ${options.emotion}`);
    return Promise.resolve();
  }

  noAngleWheelMotion(): Promise<void> {
    return Promise.resolve();
  }

  startInfraredListener(): Promise<void> {
    infraredSensor$.subscribe(event => {
      this.notifyListeners(SANBOT_INFRARE_EVENT, event);
    });
    return Promise.resolve();
  }
}

const SanbotPlugin = new SanbotPluginWeb();

export { SanbotPlugin };

registerWebPlugin(SanbotPlugin);
