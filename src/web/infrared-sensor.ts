import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

const SENSORS_COUNT = 17;
const sensors = new Array(SENSORS_COUNT).fill(0).map((_, i) => i + 1);

function getRandomSensor() {
  return sensors[Math.floor(sensors.length * Math.random())];
}

function getRandomDistance() {
  return Math.floor(Math.random() * Math.floor(99));
}

export const infraredSensor$ = interval(1000).pipe(
  map(_ => ({distance: getRandomDistance(), part: getRandomSensor()}))
);
