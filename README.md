## Installation

1. Keep screen on in main activity, otherwise your app will be killed by Sanbot

```java
    getWindow().setFlags(
      WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
      WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
    );
```

2. Standard capacitor plugin installation
