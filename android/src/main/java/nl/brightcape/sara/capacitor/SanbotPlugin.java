package nl.brightcape.sara.capacitor;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.sanbot.opensdk.function.beans.EmotionsType;

@NativePlugin()
public class SanbotPlugin extends Plugin implements SanbotInfrareListener {

    SanbotService mService;
    Boolean mShouldUnbind = false;
    PluginCall mInitCall;

    @Override
    public void onDistanceChange(int part, int distance) {
        JSObject result = new JSObject();
        result.put("part", part);
        result.put("distance", distance);
        notifyListeners("SANBOT_INFRARE_EVENT", result);
    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SanbotService.SanbotBinder binder = (SanbotService.SanbotBinder) service;
            mService = binder.getService();
            if (mInitCall != null) {
                mInitCall.success();
                mInitCall = null;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    void doBindService(PluginCall call) {
        mInitCall = call;

        if (getActivity().bindService(
                new Intent(getActivity(), SanbotService.class),
                mConnection,
                Context.BIND_AUTO_CREATE)
        ) {
            mShouldUnbind = true;
        } else {
            Log.e("SANBOT_CAPACITOR", "Error: The requested service doesn't " +
                    "exist, or this client isn't allowed access to it.");
        }
    }

    void doUnbindService() {
        if (mInitCall != null) {
            mInitCall.resolve();
        }
        if (mShouldUnbind) {
            getActivity().unbindService(mConnection);
            mShouldUnbind = false;
        }
    }

    @PluginMethod()
    public void startInfraredListener(PluginCall call) {
        mService.startInfrareListener(this);
    }

    @PluginMethod()
    public void init(PluginCall call) {
        if (mService == null) {
            doBindService(call);
        } else {
            call.success();
        }

    }

    @PluginMethod()
    public void showEmotion(PluginCall call) {
        if (!call.getData().has("emotion")) {
            call.reject("Must provide an emotion");
            return;
        }
        Integer emotion = call.getInt("emotion");
        mService.showEmotion(emotion.intValue());
        JSObject result = new JSObject();
        call.success(result);
    }

    @PluginMethod()
    public void getDeviceId(PluginCall call) {
        String deviceId = mService.getDeviceId();
        JSObject result = new JSObject();
        result.put("deviceId", deviceId);
        call.success(result);
    }

    @PluginMethod()
    public void getApiVersion(PluginCall call) {
        String version = mService.getVersion();
        JSObject result = new JSObject();
        result.put("version", version);
        call.success(result);
    }

    @PluginMethod()
    public void noAngleWheelMotion(PluginCall call) {
        if (!call.getData().has("speed")) {
            call.reject("Must provide speed");
            return;
        }
        if (!call.getData().has("duration")) {
            call.reject("Must provide duration");
            return;
        }
        Integer speed = call.getInt("speed");
        Integer duration = call.getInt("duration");
        mService.noAngleWheelMotion(speed.intValue(), duration.intValue());
        JSObject result = new JSObject();
        call.success(result);
    }

}
