package nl.brightcape.sara.capacitor;

public interface SanbotInfrareListener {
    void onDistanceChange(int part, int distance);
}
