package nl.brightcape.sara.capacitor;

import android.util.Log;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;


import com.sanbot.opensdk.base.BindBaseService;
import com.sanbot.opensdk.beans.FuncConstant;
import com.sanbot.opensdk.function.beans.EmotionsType;
import com.sanbot.opensdk.function.beans.wheelmotion.NoAngleWheelMotion;
import com.sanbot.opensdk.function.unit.HardWareManager;
import com.sanbot.opensdk.function.unit.interfaces.hardware.InfrareListener;
import com.sanbot.opensdk.function.unit.SystemManager;
import com.sanbot.opensdk.function.unit.WheelMotionManager;


public class SanbotService extends BindBaseService {
    SystemManager systemManager;
    WheelMotionManager wheelMotionManager;
    HardWareManager hardWareManager;

    public SanbotService() {
        super();
        register(SanbotService.class);

        systemManager = (SystemManager) getUnitManager(FuncConstant.SYSTEM_MANAGER);
        wheelMotionManager = (WheelMotionManager) getUnitManager(FuncConstant.WHEELMOTION_MANAGER);
        hardWareManager = (HardWareManager)getUnitManager(FuncConstant.HARDWARE_MANAGER);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    protected void onMainServiceConnected() {
    }

    public String getDeviceId() {
        return systemManager.getDeviceId();
    }

    public void noAngleWheelMotion(int speed, int durationMs) {
        NoAngleWheelMotion motion = new NoAngleWheelMotion(NoAngleWheelMotion.ACTION_FORWARD_RUN, speed, durationMs);
        wheelMotionManager.doNoAngleMotion(motion);
    }

    public void startInfrareListener(final SanbotInfrareListener listener) {
        hardWareManager.setOnHareWareListener(new InfrareListener() {
            @Override
            public void infrareDistance(int part, int distance) {
                listener.onDistanceChange(part, distance);
            }
        });
    }

    public String getVersion() {
        return systemManager.getMainServiceVersion();
    }

    public int getBatteryLevel() {
        return systemManager.getBatteryValue();
    }

    public void showEmotion(int emotion) {
        switch (emotion) {
            case 1:
                systemManager.showEmotion(EmotionsType.KISS);
                break;
            case 2:
                systemManager.showEmotion(EmotionsType.SMILE);
                break;
            case 3:
                systemManager.showEmotion(EmotionsType.GOODBYE);
                break;
            case 4:
                systemManager.showEmotion(EmotionsType.SHY);
            default:
                systemManager.showEmotion(EmotionsType.NORMAL);
        }
    }

    public class SanbotBinder extends Binder {

        public SanbotService getService(){
            return SanbotService.this;
        }
    }

    private SanbotService.SanbotBinder binder = new SanbotService.SanbotBinder();
}
