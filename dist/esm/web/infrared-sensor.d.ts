export declare const infraredSensor$: import("rxjs").Observable<{
    distance: number;
    part: number;
}>;
