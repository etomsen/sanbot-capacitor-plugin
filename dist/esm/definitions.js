export const SANBOT_INFRARE_EVENT = 'SANBOT_INFRARE_EVENT';
export var SanbotEmotion;
(function (SanbotEmotion) {
    SanbotEmotion[SanbotEmotion["none"] = 0] = "none";
    SanbotEmotion[SanbotEmotion["kiss"] = 1] = "kiss";
    SanbotEmotion[SanbotEmotion["smile"] = 2] = "smile";
    SanbotEmotion[SanbotEmotion["goodbye"] = 3] = "goodbye";
    SanbotEmotion[SanbotEmotion["shy"] = 4] = "shy";
})(SanbotEmotion || (SanbotEmotion = {}));
//# sourceMappingURL=definitions.js.map