declare module '@capacitor/core' {
    interface PluginRegistry {
        SanbotPlugin: SanbotPluginPlugin;
    }
}
export declare const SANBOT_INFRARE_EVENT = "SANBOT_INFRARE_EVENT";
export declare enum SanbotEmotion {
    none = 0,
    kiss = 1,
    smile = 2,
    goodbye = 3,
    shy = 4
}
export interface SanbotPluginPlugin {
    init(): Promise<void>;
    getDeviceId(): Promise<{
        deviceId: string;
    }>;
    getApiVersion(): Promise<{
        version: string;
    }>;
    noAngleWheelMotion(): Promise<void>;
    startInfraredListener(): Promise<void>;
    showEmotion(options: {
        emotion: SanbotEmotion;
    }): Promise<void>;
}
