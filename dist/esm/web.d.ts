import { WebPlugin } from '@capacitor/core';
import { SanbotPluginPlugin, SanbotEmotion } from './definitions';
export declare class SanbotPluginWeb extends WebPlugin implements SanbotPluginPlugin {
    constructor();
    init(): Promise<void>;
    getDeviceId(): Promise<{
        deviceId: string;
    }>;
    getApiVersion(): Promise<{
        version: string;
    }>;
    showEmotion(options: {
        emotion: SanbotEmotion;
    }): Promise<void>;
    noAngleWheelMotion(): Promise<void>;
    startInfraredListener(): Promise<void>;
}
declare const SanbotPlugin: SanbotPluginWeb;
export { SanbotPlugin };
