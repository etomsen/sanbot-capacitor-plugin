import { WebPlugin, registerWebPlugin } from '@capacitor/core';
import { SANBOT_INFRARE_EVENT } from './definitions';
import { infraredSensor$ } from './web/infrared-sensor';
export class SanbotPluginWeb extends WebPlugin {
    constructor() {
        super({
            name: 'SanbotPlugin',
            platforms: ['web']
        });
    }
    init() {
        return Promise.resolve();
    }
    getDeviceId() {
        return Promise.resolve({ deviceId: 'test-robot-id' });
    }
    getApiVersion() {
        return Promise.resolve({ version: 'test-robot-api-version' });
    }
    showEmotion(options) {
        console.log(`Showing emotion ${options.emotion}`);
        return Promise.resolve();
    }
    noAngleWheelMotion() {
        return Promise.resolve();
    }
    startInfraredListener() {
        infraredSensor$.subscribe(event => {
            this.notifyListeners(SANBOT_INFRARE_EVENT, event);
        });
        return Promise.resolve();
    }
}
const SanbotPlugin = new SanbotPluginWeb();
export { SanbotPlugin };
registerWebPlugin(SanbotPlugin);
//# sourceMappingURL=web.js.map