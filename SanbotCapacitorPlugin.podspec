
  Pod::Spec.new do |s|
    s.name = 'SanbotCapacitorPlugin'
    s.version = '0.0.1'
    s.summary = 'Sanbot Capacitor Plugin'
    s.license = 'MIT'
    s.homepage = 'git@gitlab.com:etomsen/sanbot-capacitor-plugin.git'
    s.author = 'Evgeny Tomsen'
    s.source = { :git => 'git@gitlab.com:etomsen/sanbot-capacitor-plugin.git', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.dependency 'Capacitor'
  end